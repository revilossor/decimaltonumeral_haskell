module Main where
get :: Int -> Int -> [(Int,[Char])] -> (Int, [Char])
get index decimal list = if decimal >= fst(list!!index) then list!!index else get (index + 1) decimal list 
generate :: Int -> [(Int,[Char])] -> [Char] -> [Char]
generate decimal list numeral = if decimal == 0 then numeral else generate (decimal - fst(get 0 decimal list)) list (numeral ++ snd(get 0 decimal list))
main = do
	putStrLn "Enter a positive integer to convert..."
	decimal <- getLine
	let n = generate (read decimal ::Int) [(1000,"M"),(900,"CM"),(500,"D"),(400,"CD"),(100,"C"),(90,"XC"),(50,"L"),(40,"XC"),(10,"X"),(9,"IX"),(5,"V"),(4,"IV"),(1,"I")] ""
	print n